struct QueryTree {
  1: i8 command;
  2: list<Filter> filters;
  3: list<ValueSetting> settings;
}

struct Filter {
  1: optional Filter next_;
  2: list<Comparator> comp_list;
}

struct FieldValuePair {
  1: string field;
  2: i8 val_type;
  3: i64 int_value;
  4: double real_value;
}

struct Comparator {
  1: optional Comparator next_;
  2: i8 operation;
  3: FieldValuePair fv;
}

struct ExtendedComparator {
  1: optional ExtendedComparator next_;
  2: optional ExtendedComparator connected;
  3: i8 operation;
  4: FieldValuePair fv;
}

struct ValueSetting {
  1: optional ValueSetting next_;
  2: FieldValuePair fv;
}