#ifndef LLP_3_REQUEST_HANDLER_H
#define LLP_3_REQUEST_HANDLER_H
#include "gen-c_glib/structs_types.h"

server_response_T* process_client_request(const statement_T *stmt, FILE* db);
#endif //LLP_3_REQUEST_HANDLER_H
